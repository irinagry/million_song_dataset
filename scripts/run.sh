#!/bin/bash

source scripts/config.sh $1

IN_FILE=$2

$HADOOP_HOME/bin/hadoop jar $JAR_NAME "$CLASS" $IN_FILE $OUT_DIR
