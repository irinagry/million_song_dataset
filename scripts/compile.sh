#!/bin/bash

source scripts/config.sh $1

javac -classpath $HADOOP_HOME/share/hadoop/common/hadoop-common-2.5.1.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.5.1.jar:$HADOOP_HOME/share/hadoop/common/lib/commons-cli-1.2.jar:$HADOOP_HOME/share/hadoop/common/lib/commons-logging-1.1.3.jar -d $CLASS_DIR $SRC_DIR/*.java

jar -cvf $EXE -C $CLASS_DIR .
