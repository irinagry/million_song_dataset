#!/bin/bash

# Modify here for compile:
COMPILE_CLASSES="Task"$1".java"
EXE=task"$1".jar

# Modify here for run
OUT_DIR=out_task"$1"
JAR_NAME=task"$1".jar
CLASS=msds.Task"$1"

HADOOP_HOME=/home/irina/Development/hadoop-2.5.1
CLASS_DIR=/home/irina/Dropbox/School/Master/Anul2Sem1/MASD/challenge3/bin
SRC_DIR=/home/irina/Dropbox/School/Master/Anul2Sem1/MASD/challenge3/src
