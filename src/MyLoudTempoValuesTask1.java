package msds;

import java.io.*;

import org.apache.hadoop.io.*;

/**
 * Value class for task 1
 * needed for the mapper/reducer to output loudness + tempo
 * @author irina
 *
 */
public class MyLoudTempoValuesTask1 implements Writable {
	
	DoubleWritable myLoudness;
	DoubleWritable myTempo;
	
	public MyLoudTempoValuesTask1() {
		myLoudness = new DoubleWritable(0);
		myTempo = new DoubleWritable(0);
	}
	
    public void write(DataOutput out) throws IOException {
		out.writeDouble(myLoudness.get());
        out.writeDouble(myTempo.get());
    }

    public void readFields(DataInput in) throws IOException {
    	myLoudness.set(in.readDouble());
        myTempo.set(in.readDouble());
    }

    public String toString() {
        return myLoudness.get() + " " + myTempo.get();
    }
}
