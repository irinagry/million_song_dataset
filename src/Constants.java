package msds;

public interface Constants {

	public static String[] AllGenres = { "rock", "punk", "indie", "new wave", "metal", "hardcore", "grunge",
			"rockabilly", "grind", "gothic", "emo", "screamo", "dark wave", "darkwave", "beat", "psychobilly",
			"garage", "glam", "retro", "straight edge", "no wave", "sludge", "jrock", "aor", "thrash", "jive",
			"riot grrrl", "power violence", "powerviolence", "nwobhm", "pop", "twee", "disco", "crooner", "cpop",
			"hip hop", "rap", "grime", "gangsta", "crunk", "g funk", "hyphy", "bounce", "electro", "house", "techno",
			"trance", "breakbeat", "drum and bass", "jungle", "freestyle", "gabba", "hardstyle", "new beat", "tribal",
			"eurodance", "dubstep", "grime", "electroclash", "rave", "goa", "speed core", "eurobeat", "bubblegum",
			"ghettotech", "ghetto tech", "schranz", "jazz", "swing", "bossa nova", "hard bop", "bebop", "ragtime",
			"cabaret", "honky tonk", "post-bop", "dixieland", "skiffle", "afro-cuban", "folk", "blues", "gospel",
			"delta blues", "funk", "soul", "ambient", "chill-out", "chill out", "trip hop", "lounge", "new age",
			"relax", "illbient", "meditation", "drone", "calming", "relaxation", "reggae", "dub", "ska", "rocksteady",
			"rock steady", "rasta", "ragamuffin", "ragga", "world", "country", "bluegrass", "celtic", "flamenco",
			"traditional", "calypso", "ethnic", "bolero", "african", "soca", "exotica", "zouk", "zydeco", "klezmer",
			"polka", "arabic", "vallenato", "balkan", "newgrass", "fado", "greek", "turkish", "ethno", "soukous",
			"oriental", "laika", "bouzouki", "rebetika", "latin", "samba", "salsa", "mambo", "tango", "rumba",
			"latino", "reggaeton", "bolero", "flamenco", "tropical", "merengue", "mariachi", "ranchera", "tejano",
			"boogaloo", "batucada", "banda", "bachata", "lambada", "charanga", "tex-mex", "latina", "classical",
			"symphonic", "opera", "baroque", "symphony", "r&b", "doo-wop" };

}
