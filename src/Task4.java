package msds;

import java.io.IOException;
import java.util.*;

import util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class Task4 {
	
	public static int NO_GENRES = Genre.values().length - 2;

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, MyGenreRelatednessValuesTask4> {
		Log log = LogFactory.getLog(Map.class);

		public void map(LongWritable key, Text value, OutputCollector<Text, MyGenreRelatednessValuesTask4> output,
				Reporter reporter) throws IOException {

			// File lines
			String line = value.toString();

			// Necessary properties
			Set<SongProperty> reqProperties = new HashSet<SongProperty>();
			reqProperties.add(SongProperty.ARTIST_TERMS);
			reqProperties.add(SongProperty.ARTIST_TERMS_FREQ);

			// Get song properties
			HashMap<SongProperty, String> songArtistTerms = new HashMap<SongProperty, String>();
			songArtistTerms = InputParser.getSongProperties(line, reqProperties);

			// log.info("!!!!!!!!!!!!!" +
			// songArtistTerms.get(SongProperty.ARTIST_TERMS_FREQ).toString());

			StringTokenizer tokenizerTerms = new StringTokenizer(songArtistTerms.get(SongProperty.ARTIST_TERMS), ",");
			String[] terms = new String[tokenizerTerms.countTokens()];
			int noTerms = 0;
			while (tokenizerTerms.hasMoreTokens())
				terms[noTerms++] = tokenizerTerms.nextToken();

			StringTokenizer tokenizerFreq = new StringTokenizer(songArtistTerms.get(SongProperty.ARTIST_TERMS_FREQ),
					",");
			float[] freq = new float[tokenizerFreq.countTokens()];
			int noTermsFreq = 0;
			while (tokenizerFreq.hasMoreTokens())
				freq[noTermsFreq++] = Float.parseFloat(tokenizerFreq.nextToken());

			// log.info("!!!!!!!!!!!!!" + terms[0] + "-" + terms[noTerms-1] +
			// "-" + freq[0] + "-" + freq[noTermsFreq-1]);
			// log.info("!!!!!!!!!!!!!" + noTerms + "-" + noTermsFreq);

			// Detect MyGenre
			GenreDetector gd = new GenreDetector();
			Text g = new Text(gd.detectGenre(terms, freq).toString());

			// Compute genre relatedness
			double[] genreRelatedness = new double[NO_GENRES];
			genreRelatedness = gd.compareGenres(terms, freq);
			/*for (int i = 0; i < NO_GENRES; i++)
				log.info("!!" + genreRelatedness[i] + "!!");*/

			MyGenreRelatednessValuesTask4 gr = new MyGenreRelatednessValuesTask4();
			gr.setData(genreRelatedness);

			// Collect
			if (!g.toString().equalsIgnoreCase(Genre.OTHER.toString()))
				output.collect(g, gr);

		}
	}

	public static class Reduce extends MapReduceBase implements
			Reducer<Text, MyGenreRelatednessValuesTask4, Text, MyGenreRelatednessValuesTask4> {
		public void reduce(Text key, Iterator<MyGenreRelatednessValuesTask4> values,
				OutputCollector<Text, MyGenreRelatednessValuesTask4> output, Reporter reporter) throws IOException {

			double [] relatedness = new double[NO_GENRES];
			for (int i = 0; i < NO_GENRES; i++)
				 relatedness[i] = 0;
			
			while (values.hasNext()) {
				 MyGenreRelatednessValuesTask4 gr = new MyGenreRelatednessValuesTask4(values.next().getData());
				 for (int i = 0; i < gr.relatedness.length; i++)
					 relatedness[i] += gr.relatedness[i];
			}
			
			double max = relatedness[0];
			for (int i = 0; i < relatedness.length; i++)
				if (max < relatedness[i])
					max = relatedness[i];
			for (int i = 0; i < relatedness.length; i++)
				relatedness[i] = relatedness[i] * 100 / max;
			
			
			MyGenreRelatednessValuesTask4 gr = new MyGenreRelatednessValuesTask4();
			for (int i = 0; i < gr.relatedness.length; i++)
				 gr.relatedness[i] = (int)relatedness[i];
			
			
			output.collect(key, gr);

		}
	}

	public static void main(String[] args) throws Exception {

		JobConf conf = new JobConf(Task1.class);
		conf.setJobName("task4");

		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(MyGenreRelatednessValuesTask4.class);

		conf.setMapperClass(Map.class);
		conf.setCombinerClass(Reduce.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		JobClient.runJob(conf);
	}
}
