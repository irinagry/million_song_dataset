package msds;

import java.io.IOException;
import java.util.*;

import util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class Task3 {

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, MyYearModeKeysTask3, MyLoudTempoValuesTask1> {
		private MyYearModeKeysTask3 yearMode = new MyYearModeKeysTask3();
		Log log = LogFactory.getLog(Map.class);

		public void map(LongWritable key, Text value, OutputCollector<MyYearModeKeysTask3, MyLoudTempoValuesTask1> output, Reporter reporter)
				throws IOException {

			// File lines
			String line = value.toString();

			// Necessary properties
			Set<SongProperty> reqProperties = new HashSet<SongProperty>();
			HashMap<SongProperty, String> songYear = new HashMap<SongProperty, String>();
			reqProperties.add(SongProperty.YEAR);
			reqProperties.add(SongProperty.MODE);
			reqProperties.add(SongProperty.LOUDNESS);
			reqProperties.add(SongProperty.TEMPO);
			songYear = InputParser.getSongProperties(line, reqProperties);

			// Setting output for mapper
			yearMode.myYear.set(Integer.parseInt(songYear.get(SongProperty.YEAR)));
			yearMode.myMode.set(Integer.parseInt(songYear.get(SongProperty.MODE)));
			
			MyLoudTempoValuesTask1 yearLoudnessTempo = new MyLoudTempoValuesTask1();
			yearLoudnessTempo.myLoudness.set(Double.parseDouble(songYear.get(SongProperty.LOUDNESS)));
			yearLoudnessTempo.myTempo.set(Double.parseDouble(songYear.get(SongProperty.TEMPO)));
			
			//log.info("!!!!!!!!!!!!!!!!!!!!!!!!!" + yearLoudnessTempo.toString());

			// Collect
			if (yearMode.myYear.get() != 0)
				output.collect(yearMode, yearLoudnessTempo);

		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<MyYearModeKeysTask3, MyLoudTempoValuesTask1, MyYearModeKeysTask3, MyLoudTempoValuesTask1> {
		public void reduce(MyYearModeKeysTask3 key, Iterator<MyLoudTempoValuesTask1> values, OutputCollector<MyYearModeKeysTask3, MyLoudTempoValuesTask1> output,
				Reporter reporter) throws IOException {
			
			long n = 0;
			double sumLoudness = 0;
			double sumTempo = 0;
			
			
			while (values.hasNext()) {
				MyLoudTempoValuesTask1 nextVal = values.next();
				sumLoudness += nextVal.myLoudness.get();
				sumTempo += nextVal.myTempo.get();
				n++;
			}
			
			if (n != 0) {
				sumLoudness/=n;
				sumTempo/=n;
			}
			
			MyLoudTempoValuesTask1 yearLoudnessTempo = new MyLoudTempoValuesTask1();
			yearLoudnessTempo.myLoudness.set(sumLoudness);
			yearLoudnessTempo.myTempo.set(sumTempo);
			
			output.collect(key, yearLoudnessTempo);
		}
	}

	public static void main(String[] args) throws Exception {

		JobConf conf = new JobConf(Task1.class);
		conf.setJobName("task3");

		conf.setOutputKeyClass(MyYearModeKeysTask3.class);
		conf.setOutputValueClass(MyLoudTempoValuesTask1.class);

		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		JobClient.runJob(conf);
	}
}
