package msds;

import java.io.*;

import org.apache.hadoop.io.*;

public class MyYearModeKeysTask3 implements WritableComparable<MyYearModeKeysTask3> {
   
	IntWritable myYear;
	IntWritable myMode;
	
	public MyYearModeKeysTask3() {
		myYear = new IntWritable();
		myMode = new IntWritable();
	}
	
	/* serializes the key*/
    public void write(DataOutput out) throws IOException {
        /* write all relevant members in an arbitrary order */
		out.writeInt(myYear.get());
        out.writeInt(myMode.get());
    }

    /* deserialize the key */
    public void readFields(DataInput in) throws IOException {
        /* read all relevant members in the same order they were written in */
    	myYear.set(in.readInt());
        myMode.set(in.readInt());
    }

    public int compareTo(MyYearModeKeysTask3 t) {
    	if (this.myYear.get() < t.myYear.get())
    		return -1;
    	else if (this.myYear.get() == t.myYear.get()) {
    		if (this.myMode.get() < t.myMode.get())
    			return -1;
    		else if (this.myMode.get() > t.myMode.get())
    			return 1;
    		else
    			return 0;
    	}
    	else 
    		return 1;
        /*
         * return:
         *  -1, if this < t
         *  0, if this == t
         *  1, if this > t
         */
    }

    public String toString() {
		return myYear.toString() + " " + myMode.toString();
    }
}
