package msds;

import java.io.IOException;
import java.util.*;

import util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

/**
 * The loudness war
 * @author irina
 *
 */
public class Task1 {

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, MyLoudTempoValuesTask1> {
		private IntWritable year = new IntWritable();
		Log log = LogFactory.getLog(Map.class);

		public void map(LongWritable key, Text value, OutputCollector<IntWritable, MyLoudTempoValuesTask1> output, Reporter reporter)
				throws IOException {

			// File lines
			String line = value.toString();

			// Necessary properties
			Set<SongProperty> reqProperties = new HashSet<SongProperty>();
			HashMap<SongProperty, String> songYear = new HashMap<SongProperty, String>();
			reqProperties.add(SongProperty.YEAR);
			reqProperties.add(SongProperty.LOUDNESS);
			reqProperties.add(SongProperty.TEMPO);
			songYear = InputParser.getSongProperties(line, reqProperties);

			// Setting output for mapper
			year.set(Integer.parseInt(songYear.get(SongProperty.YEAR)));
			
			MyLoudTempoValuesTask1 yearLoudnessTempo = new MyLoudTempoValuesTask1();
			yearLoudnessTempo.myLoudness.set(Double.parseDouble(songYear.get(SongProperty.LOUDNESS)));
			yearLoudnessTempo.myTempo.set(Double.parseDouble(songYear.get(SongProperty.TEMPO)));
			
			//log.info("!!!!!!!!!!!!!!!!!!!!!!!!!" + yearLoudnessTempo.toString());

			// Collect
			if (year.get() != 0)
				output.collect(year, yearLoudnessTempo);

		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<IntWritable, MyLoudTempoValuesTask1, IntWritable, MyLoudTempoValuesTask1> {
		public void reduce(IntWritable key, Iterator<MyLoudTempoValuesTask1> values, OutputCollector<IntWritable, MyLoudTempoValuesTask1> output,
				Reporter reporter) throws IOException {
			double sumLoudness = 0;
			double sumTempo = 0;
			long n = 0;
			
			while (values.hasNext()) {
				MyLoudTempoValuesTask1 nextVal = values.next();
				sumLoudness += nextVal.myLoudness.get();
				sumTempo += nextVal.myTempo.get();
				n++;
			}
			
			if (n != 0) {
				sumLoudness/=n;
				sumTempo/=n;
			}
			
			MyLoudTempoValuesTask1 yearLoudnessTempo = new MyLoudTempoValuesTask1();
			yearLoudnessTempo.myLoudness.set(sumLoudness);
			yearLoudnessTempo.myTempo.set(sumTempo);
			
			output.collect(key, yearLoudnessTempo);
		}
	}

	public static void main(String[] args) throws Exception {

		JobConf conf = new JobConf(Task1.class);
		conf.setJobName("task1");

		conf.setOutputKeyClass(IntWritable.class);
		conf.setOutputValueClass(MyLoudTempoValuesTask1.class);

		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		JobClient.runJob(conf);
	}
}
