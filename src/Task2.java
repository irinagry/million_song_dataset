package msds;

import java.io.IOException;
import java.util.*;

import util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class Task2 {

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, MyGenreLoudTempoValuesTask2> {
		private IntWritable one = new IntWritable(1);
		Log log = LogFactory.getLog(Map.class);

		public void map(LongWritable key, Text value, OutputCollector<Text, MyGenreLoudTempoValuesTask2> output, Reporter reporter)
				throws IOException {

			// File lines
			String line = value.toString();

			// Necessary properties
			Set<SongProperty> reqProperties = new HashSet<SongProperty>();
			reqProperties.add(SongProperty.ARTIST_TERMS);
			reqProperties.add(SongProperty.ARTIST_TERMS_FREQ);
			reqProperties.add(SongProperty.LOUDNESS);
			reqProperties.add(SongProperty.TEMPO);
			
			// Get song properties
			HashMap<SongProperty, String> songArtistTerms = new HashMap<SongProperty, String>();
			songArtistTerms = InputParser.getSongProperties(line, reqProperties);
			
			//log.info("!!!!!!!!!!!!!" + songArtistTerms.get(SongProperty.ARTIST_TERMS_FREQ).toString());
			
			StringTokenizer tokenizerTerms = new StringTokenizer(songArtistTerms.get(SongProperty.ARTIST_TERMS), ",");
			String [] terms = new String[tokenizerTerms.countTokens()];
			int noTerms = 0;
			while(tokenizerTerms.hasMoreTokens())
				terms[noTerms++] = tokenizerTerms.nextToken();

			StringTokenizer tokenizerFreq = new StringTokenizer(songArtistTerms.get(SongProperty.ARTIST_TERMS_FREQ), ",");
			float [] freq = new float[tokenizerFreq.countTokens()];
			int noTermsFreq = 0;
			while(tokenizerFreq.hasMoreTokens())
				freq[noTermsFreq++] = Float.parseFloat(tokenizerFreq.nextToken());
		
			
			// Detect MyGenre
			GenreDetector gd = new GenreDetector();
			Text g = new Text(gd.detectGenre(terms, freq).toString());

			//log.info("!!!!!!!!!!!!!!!!!!!!!!!!!" + g);

			MyGenreLoudTempoValuesTask2 glt = new MyGenreLoudTempoValuesTask2();
			glt.noSongs.set(1);
			glt.myLoudness.set(Double.parseDouble(songArtistTerms.get(SongProperty.LOUDNESS)));
			glt.myTempo.set(Double.parseDouble(songArtistTerms.get(SongProperty.TEMPO)));
			
			// Collect
			output.collect(g, glt);

		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<Text, MyGenreLoudTempoValuesTask2, Text, MyGenreLoudTempoValuesTask2> {
		public void reduce(Text key, Iterator<MyGenreLoudTempoValuesTask2> values, OutputCollector<Text, MyGenreLoudTempoValuesTask2> output,
				Reporter reporter) throws IOException {
			
			int noSongs = 0;
			double sumLoudness = 0;
			double sumTempo = 0;
			long n = 0;
			
			while (values.hasNext()) {
				MyGenreLoudTempoValuesTask2 nextVal = values.next();
				noSongs += nextVal.noSongs.get();
				sumLoudness += nextVal.myLoudness.get();
				sumTempo += nextVal.myTempo.get();
				n++;
			}
			
			if (n != 0) {
				sumLoudness/=n;
				sumTempo/=n;
			}
			
			MyGenreLoudTempoValuesTask2 glt = new MyGenreLoudTempoValuesTask2();
			glt.noSongs.set(noSongs);
			glt.myLoudness.set(sumLoudness);
			glt.myTempo.set(sumTempo);
			
			output.collect(key, glt);
		}
	}

	public static void main(String[] args) throws Exception {

		JobConf conf = new JobConf(Task1.class);
		conf.setJobName("task2");

		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(MyGenreLoudTempoValuesTask2.class);

		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		JobClient.runJob(conf);
	}
}
