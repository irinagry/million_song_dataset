package msds;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;

import util.Genre;

public class MyGenreRelatednessValuesTask4 implements Writable {

	double [] relatedness;
	
	public MyGenreRelatednessValuesTask4() {
		relatedness = new double[Task4.NO_GENRES];
	}
	
	public MyGenreRelatednessValuesTask4(double [] data) {
		this.relatedness = data;
	}
	
	public double[] getData() {
        return relatedness;
    }
	
	public void setData(double[] data) {
        this.relatedness = data;
    }
	
	@Override
	public void readFields(DataInput in) throws IOException {
		int length = in.readInt();
		
		relatedness = new double[length];
		
		for (int i = 0; i < length; i++)
			relatedness[i] = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		int length = 0;
		
		if (relatedness != null)
			length = relatedness.length;
	
		out.writeInt(length);
		for (int i = 0; i < length; i++)
			out.writeDouble(relatedness[i]);
	}
	
	public String toString() {
		String ret = "";
		for (int i = 0; i < Task4.NO_GENRES; i++)
			ret += relatedness[i] + " ";
		return ret;
    }

}
