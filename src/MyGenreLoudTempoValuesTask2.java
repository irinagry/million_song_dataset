package msds;

import java.io.*;

import org.apache.hadoop.io.*;

/**
 * Value class for task 2
 * needed for the mapper/reducer to output noSongs + loudness + tempo
 * @author irina
 *
 */
public class MyGenreLoudTempoValuesTask2 implements Writable {
	
	IntWritable noSongs;
	DoubleWritable myLoudness;
	DoubleWritable myTempo;
	
	public MyGenreLoudTempoValuesTask2() {
		noSongs = new IntWritable(0);
		myLoudness = new DoubleWritable(0);
		myTempo = new DoubleWritable(0);
	}
	
    public void write(DataOutput out) throws IOException {
    	out.writeInt(noSongs.get());
		out.writeDouble(myLoudness.get());
        out.writeDouble(myTempo.get());
    }

    public void readFields(DataInput in) throws IOException {
    	noSongs.set(in.readInt());
    	myLoudness.set(in.readDouble());
        myTempo.set(in.readDouble());
    }

    public String toString() {
        return noSongs.get() + " " + myLoudness.get() + " " + myTempo.get();
    }
}
