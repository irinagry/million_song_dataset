package util;

public class GenrePair implements Comparable<GenrePair> {
	
	String genre;
	double relatedness;
	
	public GenrePair(String genre, double relatedness) {
		this.genre = genre;
		this.relatedness = relatedness;
	}

	@Override
	public int compareTo(GenrePair gp) {
		return this.genre.compareToIgnoreCase(gp.genre.toString());
	}

}
