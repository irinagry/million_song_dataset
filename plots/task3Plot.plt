set terminal pdf
set style line 1 lc rgb "red"
set style line 2 lc rgb "blue"
set style fill solid
set boxwidth 0.5

# Plot 1
set xlabel "year"
set ylabel "loudness"
set output "task3Plot_loudness_bars.pdf"
set key right bottom
plot "out_task3/part-00000" using 1:($2==0?$3:1/0) title "minor" lt rgb "#FF0000" with boxes, \
	 "out_task3/part-00000" using 1:($2==1?$3:1/0) title "major" lt rgb "#0000FF" with boxes 


set output "task3Plot_loudness_points.pdf"
plot "out_task3/part-00000" using 1:($2==0?$3:1/0) title "Mode0Loudness" lt rgb "#FF0000" with points, \
	 "out_task3/part-00000" using 1:($2==1?$3:1/0) title "Mode1Loudness" lt rgb "#0000FF" with points


# Plot 2
set xlabel "year"
set ylabel "tempo"
set output "task3Plot_tempo_bars.pdf"
set key right top
plot "out_task3/part-00000" using 1:($2==0?$4:1/0) title "minor" lt rgb "#FF0000" with boxes, \
	 "out_task3/part-00000" using 1:($2==1?$4:1/0) title "major" lt rgb "#0000FF" with boxes 


set output "task3Plot_tempo_points.pdf"
plot "out_task3/part-00000" using 1:($2==0?$4:1/0) title "Mode0Tempo" lt rgb "#FF0000" with points, \
     "out_task3/part-00000" using 1:($2==1?$4:1/0) title "Mode1Tempo" lt rgb "#0000FF" with points

