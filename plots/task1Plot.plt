set terminal pdf

set xlabel "year"
set ylabel "avg_loudness"
set output "task1Plot_avg_loudness.pdf"
plot "out_task1/part-00000" using 1:2 title "Evolution of Average Loudness" lt rgb "#FF0000" with linespoints


set xlabel "year"
set ylabel "avg_tempo"
set output "task1Plot_avg_tempo.pdf"
plot "out_task1/part-00000" using 1:3 title "Evolution of Average Tempo" lt rgb "#0000FF" with linespoints
