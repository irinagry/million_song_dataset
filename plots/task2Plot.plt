set terminal pdf
set boxwidth 0.5
set style fill solid

# Plot 1
set key left top
set xlabel "genre"
set ylabel "no_songs"
set output "task2Plot_nosongs.pdf"
set xtics rotate by 45 right

plot "out_task2/part-00000" using 2:xtic(1) title "No(songs) by genres" with boxes

# Plot 2
set key right bottom
set xlabel "genre"
set ylabel "loudness"
set output "task2Plot_loudness.pdf"
set xtics rotate by 45 right

plot "out_task2/part-00000" using 3:xtic(1) title "Avg(loudness) by genres" with boxes


# Plot 3
set key left top 
set xlabel "genre"
set ylabel "loudness"
set output "task2Plot_tempo.pdf"
set xtics rotate by 45 right

plot "out_task2/part-00000" using 4:xtic(1) title "Avg(tempo) by genres" with boxes


